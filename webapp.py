import json
import humanize
import os
import requests
from typing import Dict
from flask import Flask, render_template
from datetime import datetime, timedelta

URL = 'https://api.vaksincovid.gov.my/az/'
URL_PARAM = {'action': 'listppv'}
URL_HEADERS = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36'}
CACHE_MINUTES = 30

class Cache:

    @staticmethod
    def _serialize_dt(obj):
        if isinstance(obj, datetime):
            return obj.isoformat()
        raise TypeError("Not serializable")

    def __init__(self, data):
        self.data = data
        self.fp = 'cache.json'

    def load(self):
        if os.path.exists(self.fp):
            with open(self.fp, 'r') as fh:
                self.data = json.loads(fh.read())
                self.data['timestamp'] = datetime.fromisoformat(self.data['timestamp'])
                for location in self.data['locations']:
                    for slot in location['slots']:
                        slot['day'] = datetime.fromisoformat(slot['day'])

    def exists(self):
        return self.data is not None

    def _persist(self):
        with open(self.fp, 'w+') as fh:
            fh.write(json.dumps(self.data, default=Cache._serialize_dt))

    def update(self, data):
        self.data = data
        self._persist()

    def get(self):
        return self.data


CACHE: Cache = Cache(None)
CACHE.load()

app = Flask(__name__)


def load_data() -> Dict:
    global URL
    global URL_PARAM
    global URL_HEADERS
    global CACHE

    if not CACHE.exists() or (datetime.now() - CACHE.get()['timestamp']).total_seconds()/60 > CACHE_MINUTES:
        data = requests.get(URL, params=URL_PARAM, headers=URL_HEADERS).json()
        if data['code'] == 'SUCCESS':
            data = data['data']

            # PWTC - June 06 starts, 52 slots
            pwtc = {'name': 'PWTC', 'slots': []}
            curr_date = datetime(2021,6,6)
            for i in range(0, 52):
                pwtc['slots'].append({
                    'day': curr_date,
                    'available': data[i]['available'] == 1
                })
                curr_date += timedelta(days=1)

            # UM - June 07 starts, 51 slots
            um = {'name': 'UM', 'slots': []}
            curr_date = datetime(2021,6,7)
            for i in range(52, 103):
                um['slots'].append({
                    'day': curr_date,
                    'available': data[i]['available'] == 1
                })
                curr_date += timedelta(days=1)

            # UKM - June 07 starts, 51 slots
            ukm = {'name': 'UKM', 'slots': []}
            curr_date = datetime(2021,6,7)
            for i in range(103, 154):
                ukm['slots'].append({
                    'day': curr_date,
                    'available': data[i]['available'] == 1
                })
                curr_date += timedelta(days=1)

            # IDCC - June 07 starts, 51 slots
            idcc = {'name': 'IDCC Shah Alam', 'slots': []}
            curr_date = datetime(2021,6,7)
            for i in range(154, 205):
                idcc['slots'].append({
                    'day': curr_date,
                    'available': data[i]['available'] == 1
                })
                curr_date += timedelta(days=1)

            CACHE.update({
                'timestamp': datetime.now(),
                'locations': [pwtc, um, ukm, idcc],
            })

    return CACHE.get()

@app.route('/az-vac')
def index():
    cache = load_data()
    return render_template(
        'index.html', locations=cache['locations'],
        timestamp=humanize.naturaltime(cache['timestamp']))

app.config['TEMPLATES_AUTO_RELOAD'] = True
