FROM python:3.9.5-slim-buster

ADD templates/. /app/templates/
ADD requirements.txt webapp.py wsgi.py /app/

WORKDIR /app

RUN pip install -r requirements.txt

CMD ["gunicorn", "--workers", "1", "--bind", "0.0.0.0:80", "wsgi:app"]
